import java.io.IOException;
import java.util.regex.Pattern;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.log4j.Logger;

public class Collisions extends Configured implements Tool {

    private static final Logger LOG = Logger.getLogger(Collisions.class);

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Collisions(), args);
        System.exit(res);
    }

    public int run(String[] args) throws Exception {
        Job job = new Job(getConf());

        job.setJarByClass(this.getClass());
        // Use TextInputFormat, the default unless job.setInputFormatClass is used
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        job.setMapperClass(Map.class);
        job.setCombinerClass(Combiner.class);
        job.setReducerClass(Reduce.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static class Combiner extends Reducer<Text, IntWritable, Text, IntWritable> {
        @Override
        public void reduce(Text key, Iterable<IntWritable> counts, Context context)
                throws IOException, InterruptedException {
            int sum = 0;
            for (IntWritable count : counts) {
                sum += count.get();
            }
            context.write(key, new IntWritable(sum));
        }
    }

    public static class Record{
        int currentYear;
        String zipCode;
        String onStreet;
        String crossStreet;
        String offStreet;
        String noPedestrianInjured;
        String noPedestrianKilled;
        String noCyclistInjured;
        String noCyclistKilled;
        String noMotoristInjured;
        String noMororistKilled;

        Record(String[] line){
            this.currentYear = Integer.parseInt(line[0].substring(line[0].lastIndexOf('/') + 1, line[0].lastIndexOf('/') + 5));
            this.zipCode = line[2];
            this.onStreet =line[6];
            this.crossStreet = line[7];
            this.offStreet = line[8];
            this.noPedestrianInjured = line[11];
            this.noPedestrianKilled = line[12];
            this.noCyclistInjured = line[13];
            this.noCyclistKilled = line[14];
            this.noMotoristInjured = line[15];
            this.noMororistKilled = line[16];
            
        }
    }


    public static class Map extends Mapper<LongWritable, Text, Text, IntWritable> {


        public void map(LongWritable key, Text value, Context context)
                throws IOException, InterruptedException {
            try {
                if (key.get() == 0)
                    return;
                else {
                    String[] line = value.toString().split(",");
                    Record record = new Record(line);
                    if (record.currentYear > 2012 && !record.zipCode.isEmpty()) {
                        if (!record.noPedestrianInjured.isEmpty() && !record.noPedestrianInjured.equals("0")) {
                            if (!record.onStreet.isEmpty()) {
                                context.write(new Text(record.zipCode + "\t"  +record.onStreet + "\t" + "PEDESTRIAN\tINJURED"), new IntWritable(Integer.parseInt(record.noPedestrianInjured)));
                            }
                            if (!record.crossStreet.isEmpty()) {
                                context.write(new Text(record.zipCode + "\t" + record.crossStreet + "\t" + "PEDESTRIAN\tINJURED"), new IntWritable(Integer.parseInt(record.noPedestrianInjured)));
                            }
                            if (!record.offStreet.isEmpty()) {
                                context.write(new Text(record.zipCode + "\t" + record.offStreet+ "\t" + "PEDESTRIAN\tINJURED"), new IntWritable(Integer.parseInt(record.noPedestrianInjured)));
                            }
                        }
                        if (!record.noPedestrianKilled.isEmpty() && !record.noPedestrianKilled.equals("0")) {
                            if (!record.onStreet.isEmpty()) {
                                context.write(new Text(record.zipCode + "\t"  + record.onStreet + "\t" + "PEDESTRIAN\tKILLED"), new IntWritable(Integer.parseInt(record.noPedestrianKilled)));
                            }
                            if (!record.crossStreet.isEmpty()) {
                                context.write(new Text(record.zipCode + "\t" + record.crossStreet + "\t" + "PEDESTRIAN\tKILLED"), new IntWritable(Integer.parseInt(record.noPedestrianKilled)));
                            }
                            if (!record.offStreet.isEmpty()) {
                                context.write(new Text(record.zipCode + "\t" + record.offStreet + "\t" + "PEDESTRIAN\tKILLED"), new IntWritable(Integer.parseInt(record.noPedestrianKilled)));
                            }
                        }
                        if (!record.noCyclistInjured.isEmpty() && !record.noCyclistInjured.equals("0")) {
                            if (!record.onStreet.isEmpty()) {
                                context.write(new Text(record.zipCode + "\t"  + record.onStreet + "\t" + "CYCLIST\tINJURED"), new IntWritable(Integer.parseInt(record.noCyclistInjured)));
                            }
                            if (!record.crossStreet.isEmpty()) {
                                context.write(new Text(record.zipCode + "\t" + record.crossStreet + "\t" + "CYCLIST\tINJURED"), new IntWritable(Integer.parseInt(record.noCyclistInjured)));
                            }
                            if (!record.offStreet.isEmpty()) {
                                context.write(new Text(record.zipCode + "\t" + record.offStreet + "\t" + "CYCLIST\tINJURED"), new IntWritable(Integer.parseInt(record.noCyclistInjured)));
                            }

                        }
                        if (!record.noCyclistKilled.isEmpty() && !record.noCyclistKilled.equals("0")) {
                            if (!record.onStreet.isEmpty()) {
                                context.write(new Text(record.zipCode + "\t"  + record.onStreet + "\t" + "CYCLIST\tKILLED"), new IntWritable(Integer.parseInt(record.noCyclistKilled)));
                            }
                            if (!record.crossStreet.isEmpty()) {
                                context.write(new Text(record.zipCode + "\t" + record.crossStreet + "\t" + "CYCLIST\tKILLED"), new IntWritable(Integer.parseInt(record.noCyclistKilled)));
                            }
                            if (!record.offStreet.isEmpty()) {
                                context.write(new Text(record.zipCode + "\t" + record.offStreet + "\t" + "CYCLIST\tKILLED"), new IntWritable(Integer.parseInt(record.noCyclistKilled)));
                            }
                        }
                        if (!record.noMotoristInjured.isEmpty() && !record.noMotoristInjured.equals("0")) {
                            if (!record.onStreet.isEmpty()) {
                                context.write(new Text(record.zipCode + "\t"  + record.onStreet + "\t" + "MOTORIST\tINJURED"), new IntWritable(Integer.parseInt(record.noMotoristInjured)));
                            }
                            if (!record.crossStreet.isEmpty()) {
                                context.write(new Text(record.zipCode + "\t" + record.crossStreet + "\t" + "MOTORIST\tINJURED"), new IntWritable(Integer.parseInt(record.noMotoristInjured)));
                            }
                            if (!record.offStreet.isEmpty()) {
                                context.write(new Text(record.zipCode + "\t" + record.offStreet + "\t" + "MOTORIST\tINJURED"), new IntWritable(Integer.parseInt(record.noMotoristInjured)));
                            }
                        }
                        if (!record.noMororistKilled.isEmpty() && !record.noMororistKilled.equals("0")) {
                            if (!record.onStreet.isEmpty()) {
                                context.write(new Text(record.zipCode + "\t"  + record.onStreet + "\t" + "MOTORIST\tKILLED"), new IntWritable(Integer.parseInt(record.noMororistKilled)));
                            }
                            if (!record.crossStreet.isEmpty()) {
                                context.write(new Text(record.zipCode + "\t" + record.crossStreet + "\t" + "MOTORIST\tKILLED"), new IntWritable(Integer.parseInt(record.noMororistKilled)));
                            }
                            if (!record.offStreet.isEmpty()) {
                                context.write(new Text(record.zipCode + "\t" + record.offStreet + "\t" + "MOTORIST\tKILLED"), new IntWritable(Integer.parseInt(record.noMororistKilled)));
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static class Reduce extends Reducer<Text, IntWritable, Text, IntWritable> {
        @Override
        public void reduce(Text key, Iterable<IntWritable> counts, Context context)
                throws IOException, InterruptedException {
            int sum = 0;
            for (IntWritable count : counts) {
                sum += count.get();
            }
            context.write(key, new IntWritable(sum));
        }
    }
}
