wget http://www.cs.put.poznan.pl/kjankiewicz/bigdata/projekt1/zips-boroughs.csv
wget http://www.cs.put.poznan.pl/kjankiewicz/bigdata/projekt1/NYPD_Motor_Vehicle_Collisions.csv

hadoop fs -mkdir -p bigdata/proj1/input
hadoop fs -copyFromLocal zips-boroughs.csv bigdata/proj1/input/zips-boroughs.csv
hadoop fs -copyFromLocal NYPD_Motor_Vehicle_Collisions.csv bigdata/proj1/input/NYPD_Motor_Vehicle_Collisions.csv

hadoop jar collisions.jar Collisions bigdata/proj1/input bigdata/proj1/output

pig run.pig

hadoop fs -copyToLocal bigdata/proj1/output/pig/part-r-00000 result.json
hadoop fs -rm -r bigdata

nano result.json
