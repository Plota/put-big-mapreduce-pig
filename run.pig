raw_crashes0 = LOAD 'bigdata/proj1/output/part-r-00000' USING PigStorage('\t') as (zip_code:int, street:chararray, person_type:chararray, injury_character:chararray, no_victims:int);
raw_crashes1 = LOAD 'bigdata/proj1/output/part-r-00001' USING PigStorage('\t') as (zip_code:int, street:chararray, person_type:chararray, injury_character:chararray, no_victims:int);
raw_crashes2 = LOAD 'bigdata/proj1/output/part-r-00002' USING PigStorage('\t') as (zip_code:int, street:chararray, person_type:chararray, injury_character:chararray, no_victims:int);
raw_crashes3 = LOAD 'bigdata/proj1/output/part-r-00003' USING PigStorage('\t') as (zip_code:int, street:chararray, person_type:chararray, injury_character:chararray, no_victims:int);
raw_crashes4 = LOAD 'bigdata/proj1/output/part-r-00004' USING PigStorage('\t') as (zip_code:int, street:chararray, person_type:chararray, injury_character:chararray, no_victims:int);

raw_crashes = UNION raw_crashes0, raw_crashes1, raw_crashes2, raw_crashes3, raw_crashes4;
zip_boroughs = LOAD 'bigdata/proj1/input/zips-boroughs.csv' USING PigStorage(',') as (zip_code:int,borough:chararray);



ranked_zip_boroughs = RANK zip_boroughs;


raw_zip_boroughs =  FILTER ranked_zip_boroughs BY $0>1;


crashes_with_boroughs = JOIN raw_crashes BY zip_code, raw_zip_boroughs by zip_code;
crashes_with_boroughs_filtered = FILTER crashes_with_boroughs BY borough == 'MANHATTAN';

pre_killed = FILTER crashes_with_boroughs_filtered BY injury_character == 'KILLED';
pre_injured = FILTER crashes_with_boroughs_filtered BY injury_character == 'INJURED';

pre_killed_prepared = FOREACH pre_killed GENERATE street, person_type, injury_character, no_victims as killed:int;
pre_injured_prepared = FOREACH pre_injured GENERATE street, person_type, injury_character, no_victims as injured:int;

pre_killed_grouped = GROUP pre_killed_prepared BY (street, person_type);
pre_injured_gruped = GROUP pre_injured_prepared BY (street, person_type);


final_killed = FOREACH pre_killed_grouped  GENERATE $0.street, $0.person_type, SUM($1.killed) as sum_kill:int;
final_injured = FOREACH pre_injured_gruped GENERATE $0.street, $0.person_type, SUM($1.injured) as sum_inj:int;


pre_final_result = JOIN final_killed BY (street, person_type), final_injured BY (street, person_type);



pre_final_sum = FOREACH pre_final_result GENERATE final_killed::raw_crashes::street as street:chararray, final_killed::raw_crashes::person_type as person_type:chararray, sum_kill, sum_inj, sum_kill+sum_inj as sum:int;

pre_final_grouped = GROUP pre_final_sum  BY person_type;


final= FOREACH pre_final_grouped {
ordered = ORDER pre_final_sum BY sum DESC;
limited = LIMIT ordered 3;
result = FOREACH limited GENERATE street as street, person_type as person_type, sum_kill as sum_killed, sum_inj as sum_injured;
GENERATE result  ;
};
STORE final into 'bigdata/proj1/output/pig/' USING JsonStorage();
